# Remise du travail pratique 1

## Identification

- Cours      : Utilisation et administration des systèmes informatiques
- Sigle      : INF1070
- Session    : Hiver 2019
- Groupe     : `011`
- Enseignant : `Lucas Bajolet`
- Auteur     : `Jefferson Faustin` (`FAUJ08079202`)
- Auteur     : `Hugues Arly Chanoine` (`CHAH26108905`)




## Solution de la mission 1 d'exemple

### État de la mission : résolue

### Démarche

J'ai utilisé la commande `echo` pour afficher les trois lignes.
J'ai utilisé l'option `-e` pour interpréter les caractères `\n` correctement.
J'ai redirigé le résultat dans le fichier `exemple.txt` :

```sh
echo -e 'alpha\nbeta\ngamma\n' > exemple.txt
```

Ensuite, en entrant

```sh
gash check
```

la mission a été validée, ce qui a conclu cette mission.




## Solution de la mission 2

### État de la mission : résolue

### Démarche

Nous avons utilisé la commande time pour d'obtenir le "real time" de chaque programme afin de voir lequel s'exécute le plus rapidement.

```sh
time ./hpl
time ./ior
time ./mdtest
```
le programme exécute le plus vite est mdtest.

## Solution de la mission 3

### État de la mission : résolue

### Démarche

Nous avons commencé par décompresser le dossier, donc nous avons fait la commande `gzip -d cassé.tar.gz` suivi de la commande `tar -xf cassé.tar`. Par la suite, nous avons fait `ls` pour trouver les fichiers kaa, kab, kac, kad et kae. Nous avons ensuite fait `cat ka* > donald.mp4` afin de concaténer et mettre le produit final dans le fichier donald.mp4



## Solution de la mission 4

### État de la mission : résolue

### Démarche

`mkdir -p INF1070/{Ecrit/{Examen,Quiz},TP} && touch INF1070/Ecrit/Examen/intra && touch INF1070/Ecrit/Quiz/quiz{1,2} && touch INF1070/TP/tp{1,2} && touch INF1070/Ecrit/Examen/intra && touch INF1070/Ecrit/Examen/final` nous permet de faire l'arborescence voulue.


## Solution de la mission 5

### État de la mission : résolue

### Démarche

Pour nous permettre de trouver la différence entre les 3 fichiers, nous avons tout simplement utilisé la commande `diff3`.

```sh
diff3 naf-naf nif-nif nouf-nouf
```

Ce qui nous permet de découvrir que nouf-nouf est le fichier qui possède des différences.


## Solution de la mission 6

### État de la mission : résolue

### Démarche

Après une courte recherche sur Google nous avons découvert que le décalage orignal est un décalage `rot` de 3. Cela nous a permis de faire la commande suivante afin d'obtenir le vrai texte.

```sh
cat cipher | tr a-zA-Z d-za-cD-ZA-C > normal
```

## Solution de la mission 7

### État de la mission : résolue

### Démarche

Afin de retrouver le jour de semaine correspondant à la date du 26 décembre 1984 nous avons fait la commande suivante:

```sh
date -d "1984-12-26"
```

Qui nous permet de savoir que cette date correspond à mercredi.


## Solution de la mission 8

### État de la mission : résolue

### Démarche

Afin de trouver l'acrostiche nous avons tous simplement utilisé les commandes suivantes: `tac` qui inverse l'ordre des lignes, `rev` qui inverse les lignes et `cut` afin d'obtenir seulement la première lettre de chaque lignes.

```sh
rev acrostiche.txt | tac | cut -b 1
```

Qui nous donne le mot `ESCARGOT`.


## Solution de la mission 9

### État de la mission : résolue

### Démarche

Pour déplacer tous les fichiers avec un titre qui possède le chiffre 2 vers un dossier nommé menace. La commande `mv` nous permet de déplacer un fichier dans le répertoire voulu. Du `glob` qui ressemble beaucoup à de l'expression régulière, nous permet de créer des règles.

```sh
mv uncleHome/*[2]* menace/
```


## Solution de la mission 10

### État de la mission : résolue

### Démarche

afin de bouger les fichiers indésirable dans les dossiers voulu nous avons utilisé du "glob" et la commande `mv` comme vue dans la mission précédente.


```sh
mv [2]* premiereMenace/
mv *[2]? derniereMenace/
mv *[2]*[2]* fourbeMenace/
mv *[2]* menace/
```

## Solution de la mission 11

### État de la mission : résolue

### Démarche

afin de changer les droit, comme la mission le désire, nous avons fait l'usage de la commande `chmod` qui nous a permis de changer les droits pour ceux voulus.

```sh
chmod -R 710 assiette couteau table
chmod -R 700 gratin poire tarte tourte
```


## Solution de la mission 12

### État de la mission : résolue

### Démarche

Comme expliqué au dernier numéro, nous pouvons changer la permission avec la commande `chmod`. Lorsque jumelé avec du glob, cela nous permet d'obtenir le résultat souhaité.

```sh
chmod 511 *a*
chmod 700 *r*
chmod 222 ing*
```


## Solution de la mission 13

### État de la mission : résolue

### Démarche

Afin de lister les 6 répertoires pour rassurer le chef nous avons fait comme suit:

Pour avoir seulement avoir les droits d'accès des répertoires, nous avons utilisé la commande `ls` avec l'option `l` pour voir les permissions et l'option `d` pour lister seulement les répertoires avec le glob `*/` pour afficher les répertoires dans le répertoire courant. Nous avons utilisés la commande `cut` avec l'option `c` pour prendre seulement le deuxième octet jusqu'au dixième. Grâce à cela, nous avons réussi  à avoir seulement les permissions et  rediriger le résultat dans le fichier `1we_are_safe`.

```sh
ls -dl */|cut -c 2-10 > 1we_are_safe
```

## Solution de la mission 14

### État de la mission : résolue

### Démarche

Pour pouvoir créez tous les fichiers textes et les liens symboliques nous avons utilisé les commandes suivantes.

en premier la command `echo "vrai" > valeur_vrai.txt` pour crée les fichiers et ensuite `echo "faux" > valeur_faux.txt`
pour les valeurs fausse.

Ensuite les commandes suivantes afin de crée tous les liens symboliques vers les fichiers.

```sh
ln -s valeur_vrai.txt faux_ou_vrai
ln -s valeur_vrai.txt vrai_et_vrai
ln -s valeur_vrai.txt vrai_ou_vrai
ln -s valeur_vrai.txt vrai_ou_faux
ln -s valeur_faux.txt faux_ou_faux
ln -s valeur_faux.txt faux_et_vrai
ln -s valeur_faux.txt faux_et_faux
ln -s valeur_faux.txt vrai_et_faux
```


## Solution de la mission 15

### État de la mission : résolue

### Démarche

On commence par faire la cartographie sous forme de dossiers avec `mkdir`.

```sh
mkdir {Orcanie,Calédonie,Irlande,Carmélide,Galles,Logres,Armorique,Gaunes,Vannes,Aquitaine}
```

Ensuite, on fais `ln` qui nous permet de créer des liens entre fichiers. On rajoute `-s` afin de créer des liens symbolique. Les chemins sont relatif.


```sh
ln -s ../Calédonie ./Orcanie/sud &&
ln -s ../Orcanie ./Calédonie/nord &&
ln -s ../Carmélide ./Calédonie/sud &&
ln -s ../Irlande ./Calédonie/sud-ouest &&
ln -s ../Calédonie ./Irlande/nord-est &&
ln -s ../Carmélide ./Irlande/est &&
ln -s ../Galles ./Irlande/sud-est &&
ln -s ../Calédonie ./Carmélide/nord &&
ln -s ../Logres ./Carmélide/sud &&
ln -s ../Irlande ./Carmélide/ouest &&
ln -s ../Irlande ./Galles/nord-ouest &&
ln -s ../Logres ./Galles/est &&
ln -s ../Carmélide ./Logres/nord &&
ln -s ../Galles ./Logres/ouest &&
ln -s ../Gaunes ./Logres/sud &&
ln -s ../Vannes ./Armorique/sud &&
ln -s ../Gaunes ./Armorique/est &&
ln -s ../Logres ./Gaunes/nord &&
ln -s ../Armorique ./Gaunes/nord-ouest &&
ln -s ../Vannes ./Gaunes/ouest &&
ln -s ../Aquitaine ./Gaunes/sud &&
ln -s ../Armorique ./Vannes/nord &&
ln -s ../Gaunes ./Vannes/est &&
ln -s ../Gaunes ./Aquitaine/nord
```


## Solution de la mission 16

### État de la mission : résolue, partiellement résolue, non résolue

### Démarche

Premièrement, pour trouver le nombre de garçons et de filles il y a dans la liste on fait un `grep` qui affiche les lignes correspondant à un motif donne mais on rajoute `-c` qui nous donnes le  nombre de ligne correspondantes.

```sh
grep -c 'garçon' prenoms.txt && grep -c 'fille' prenoms.txt
```

Deuxièmement, pour trouver en quelle année il y a eu le plus de nouveau ne, on fait un `cut` `-f 1` qui cherche dans la première rangé. ensuite, `-d ' '` on marque que la première rangé fini lorsqu'on rencontre un espace. pour finir, on ajoute une `sort` pour jumeler les naissance pour les filles et les garçons et uniq -c afin d'avoir le nombre de ligne unique.

```sh
cut -f 1 -d ' ' prenoms.txt | sort | uniq -c
```

Finalement, pour trouver le nom le plus populaire, il suffit de faire la même chose que pour les année mais on ajoute un autre `sort -n` qui nous permet de mettre la liste en ordre croissant et ensuite un tail -1 qui nous donne le chiffre le plus grand nombre d'occurrence d'un nom.

```sh
cut -f 3 -d ' '  prenoms.txt | sort -n | uniq -c | sort -n | tail  -1
```


## Solution de la mission 17

### État de la mission : résolue

### Démarche

Pour trouver tous les bêtes qui possèdes les attributs recherchés. On commence par rentrer dans le dossiers voulu avec `cd chasse`. Après, j'utilise `grep` afin de trouver le attributs désirer à l’aide de l'expression régulière.

```sh
grep -lrz 'Présence Litosphérique.*\n*Présence Craicique\|Présence Craicique.*\n*Présence Litosphérique' * | sort > bestiaire.txt
```

On rajoute r pour rendre cela récursif afin de passer par tous les fichier. l pour retrouver les fichiers qui match le regex. Pour finir, Le z rajoute un byte zéro a la fin de chaque ligne.

je mets un pipe au milieu avec les attributs inverser pour couvrir la possibilité qu'un attribut apparait avant l’autre. Le résultat est ensuite trié en ordre alphabétique et est mis dans un fichier nommé bestiaire.txt.


## Solution de la mission 18

### État de la mission : résolue

### Démarche
Pour trouver tout les fichiers, que nous devons dépla, nous avons utilliser la commande `find` avec l'option `-type f` pour déterminer qu'on veut des fichiers simple. Nous avons notre séquence en parenthèses, pour chercher différent fichiers avec l'option `-name` et l'option `-o` pour chercher plus qu'un fichier. Nous avons utilisé `exec`, pour exécuter la commande `mv` pour déplacer les trouver dans le répertoires  Quarantaine.



```sh
find Rapports/* -type f \( -name "annee_*.log" -o -name "global.log" \) -exec mv {}  ~/Quarantaine/ \;
```



## Solution de la mission 19

### État de la mission : résolue

### Démarche

Pour la mission 19 nous avons décidé de tous d’abord retirer tous les fichiers qui se trouvait dans les sous-répertoires du répertoire Artefacts.

```sh
find artefacts/*/* -type f | xargs mv -t artefacts/
```

Ensuite, nous avons supprimer les sous-répertoires maintenant vide a l'aide de la commande `rmdir *`. maintenant qu'il ne reste que les fichiers voulu nous pouvons faire.

```sh
rmdir artefacts/*
```

Les fichiers qui se retrouve dans le dossier Artefacts est déjà en ordre alphabétique, donc pas besoin d'utiliser `sort`.
On fait la commande suivante pour extraire les 3 premiers caractères de chaque fichier et de le mettre dans un fichier nommer prophetie.txt.

```sh
head -qc 3 artefacts/* > prophétie.txt
```
